export enum RedisDbInstance {
  GUESTS = 0,
  LOBBIES = 1,
  GAMES = 2,
}
