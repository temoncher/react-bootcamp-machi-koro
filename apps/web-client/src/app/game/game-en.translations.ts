export const GAME_EN_TRANSLATIONS = {
  passButtonText: 'Pass',
  finishTurnButtonText: 'Finish turn',
  rollDiceButtonText: 'RollDice',
  startGameButtonText: 'Start game',
};
