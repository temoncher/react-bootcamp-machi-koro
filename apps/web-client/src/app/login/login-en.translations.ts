export const LOGIN_EN_TRANSLATIONS = {
  tryAsGuestButtonText: 'Try as a guest',
  guestFormTitle: 'Login',
  guestFormLableUsername: 'Pick a username',
  guestFormButton: 'Submit',
  usernameEmptyErrorMessage: 'Username should not be empty',
  usernameFirstCharacterErrorMessage: 'Username should start with a character',
  usernameRestrictedCharactersErrorMessage: 'The username should only consist of Latin characters, spaces and hyphens',
};
