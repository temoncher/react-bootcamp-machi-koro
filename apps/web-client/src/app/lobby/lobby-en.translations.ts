export const LOBBY_EN_TRANSLATIONS = {
  startNewGameButtonText: 'Start a new game',
  playersSectionTitle: 'Players',
  guestCardSubtitleText: 'Logged in as guest',
  leaveLobbyButtonText: 'Leave lobby',
};
